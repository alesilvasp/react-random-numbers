import "./style.css";

function WinMsg({ seven }) {
  if (seven === 7) {
    return (
      <div className="container_msg">
        <span className="container_msg__winner">
          Parabéns, acertou o <strong>7</strong>
        </span>
      </div>
    );
  }
  return (
    <div className="container_msg">
      <span>Quase. Tente de novo...</span>
    </div>
  );
}

export default WinMsg;
