import './App.css';
import { useState } from 'react';
import WinMsg from './components/WinMsg';


function App() {

  const [number, setNumber] = useState(Math.floor(Math.random() * 12) + 1)

  function randomNumber () {
    let dice1 = Math.floor(Math.random() * 6) + 1;
    let dice2 = Math.floor(Math.random() * 6) + 1;
    let result = dice1 + dice2;
    return setNumber(result)
  }

  return (
    <div className="App">
     <div className='App-header'>
       <p className="title">Role os dados para acertar o número 7</p>
       {number}
       <button className='Roll' onClick={randomNumber}>Rolar os dados</button>
       <WinMsg seven={number}/>
     </div>
    </div>
  );
}

export default App;
